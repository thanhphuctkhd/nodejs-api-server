## Quick start

- Make sure you are using Node 16
- Run ```npm install```
- Run ```node server.js``` or ```npm start```
- Console should log out that "Server is running on 3000". The server is now online and ready to response.

## Available APIs
- POST ```"http://localhost:3000/upload"```:
  - This API allow user to upload file to server
  - Query: ```overwrite```: When ```overwrite``` == 'true', the uploaded file will replace the file with the same
name. When ```overwrite``` == 'false' or undefined, the uploaded file will be saved with a new name.
  - Body: form-data:```file```: The file to be uploaded.
- GET ```"http://localhost:3000/list"```:
  - This API returns all files those have been uploaded to server
  - Query: None required
  - Body: None required
- GET ```"http://localhost:3000/file/:fileName"```:
  - This API returns the content of a specific file.
  - Params: ```:fileName``` : The exact name of the file that you want to retrieve
