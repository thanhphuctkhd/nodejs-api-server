const express = require('express');
const fs = require('fs');
const multer = require('multer');
const STORAGE_DIRECTORY = `${__dirname}/uploads/`;

function getExistingFile(fileName) {
    try {
        const existingFile = fs.readFileSync(`${STORAGE_DIRECTORY}${fileName}`, {encoding: "utf-8"});
        return existingFile;
    } catch (err) {
        if (err.code === 'ENOENT') {
            return null;
        } else {
            throw err;
        }
    }
}

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        if (!fs.existsSync(STORAGE_DIRECTORY)) {
            fs.mkdirSync(STORAGE_DIRECTORY);
        }
        cb(null, STORAGE_DIRECTORY);
    },
    filename: (req, file, cb) => {
        const {overwrite} = req.query;
        if (overwrite == null || overwrite.toLowerCase() === 'false' || overwrite.toLowerCase() === 'f') {
            let existingFile = getExistingFile(file.originalname);
            if (existingFile) {
                let fileNameIndex = 0;
                while (existingFile) {
                    fileNameIndex++;
                    const extensionStartIndex = file.originalname.lastIndexOf('.');
                    let newFileName;
                    if (extensionStartIndex > -1) {
                        newFileName = file.originalname.slice(0, extensionStartIndex) + `(${fileNameIndex})` + file.originalname.slice(extensionStartIndex);
                    } else {
                        newFileName = file.originalname + `(${fileNameIndex})`;
                    }
                    existingFile = getExistingFile(newFileName);
                    if (!existingFile) {
                        cb(null, newFileName);
                    }
                }
            } else {
                cb(null, file.originalname)
            }
        } else {
            cb(null, file.originalname);
        }
    }
})
const upload = multer({storage: storage}).single('file');

const app = express();
const port = process.env.PORT || 3000;

app.post(
    "/upload",
    function (req, res) {
        upload(req, res, function (err) {
            if (err) {
                return res.status(400).json({
                    message: err.message
                })
            }
            const file = req.file;
            res.status(200).json({
                message: "Upload succeeded!",
                filename: file.filename,
                mimetype: file.mimetype,
                originalname: file.originalname,
                size: file.size,
                fieldname: file.fieldname
            })
        })
    });

app.get("/list", function (req, res) {
    try {
        const files = fs.readdirSync(STORAGE_DIRECTORY);
        res.status(200).json({
            files: files,
            message: 'File list retrieved!'
        })
    } catch (err) {
        if (err.code === 'ENOENT') {
            fs.mkdirSync(STORAGE_DIRECTORY);
            res.status(200).json({
                files: [],
                message: 'File list retrieved!'
            })
        } else {
            res.status(400).json({
                message: err.message
            })
        }
    }

})

app.get("/file/:fileName", function (req, res) {
    const {fileName} = req.params;
    try {
        const file = getExistingFile(fileName);
        if (!file) {
            res.status(400).json({
                message: `Cannot find file with name: ${fileName}`
            })
        } else {
            res.status(200).json({
                message: 'File retrieved!',
                fileName: fileName,
                file: file,
            })
        }
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }

})

app.listen(port, () => {
    console.log(`Server is running on ${port}`)
})

